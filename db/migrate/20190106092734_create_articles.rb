class CreateArticles < ActiveRecord::Migration[5.1]
  def change
    create_table :articles do |t|
      t.string :title
      t.text :content
      t.integer :category_id
      t.integer :admin_id
      t.string :image
      t.date :publish_date
      t.boolean :status, default: true

      t.timestamps
    end
  end
end
