class CreateGalleries < ActiveRecord::Migration[5.1]
  def change
    create_table :galleries do |t|
      t.string :image
      t.boolean :status, default: true
      t.integer :admin_id

      t.timestamps
    end
  end
end
