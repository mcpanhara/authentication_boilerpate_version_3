# Gift Website

A group of passionate Cambodia programmer, database administrator, system administrator and tech startup founder. A great place for sharing knowledge and networking.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Installing

after clone the project

```
$ bundle install
```
after you bundle all dependencies you have to create your .env file on the root directory of the project and copy and paste the code below or copy the ENVIROMENT_VARIABLE from the .env.example and setup your ENVIROMENT_VARIABLE

```
SECRET_DATABASE_NAME = yourdbusername
SECRET_PASSWORD = yourpassworddb

SECRET_USERNAME = yourusername
SECRET_ADMIN_EMAIL = youremail
SECRET_ADMIN_PASSWORD = yourpassword
SECRET_EMAIL_PROVIDER = youremail
SECRET_PASWORD_PROVIDER = youremailpassword


SECRET_USER_USERNAME = yourusername
SECRET_USER_EMAIL = youruseremail
SECRET_USER_PASSWORD = yourpassword

```

than follow the command below

```
$ rake db:create
```
for create a admin user you have to follow the command below

```
$ rake db:reset or rake db:seed
```

Now You Can You Run

```
$ rails s
```
Open Your Browser run localhost:3000 for the site and localhost:3000/admin for the administrator
```
localhost:3000/

localhost:3000/admin

username: yourusername
password: yourpassword
```
