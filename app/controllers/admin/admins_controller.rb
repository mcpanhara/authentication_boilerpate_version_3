class Admin::AdminsController < ApplicationController
    before_action :authenticate_admin!
    before_action :admin_only?

    def index
      @admins = Admin.all
      @admin = Admin.new
    end

    def show
      @admin = Admin.find(params[:id])
    end

    def new

    end

    def create
      @admin = current_admin.admins.build(admin_params)
      if @admin.save
        respond_to do |format|
          flash.now[:notice] = "Successful Created"
          format.html { redirect_to admin_admins_path }
          format.js
        end
      else
        respond_to do |format|
          flash.now[:alert] = "Please fill the field blank or admin Duplicated"
          format.html { redirect_to admin_admins_path }
          format.js { render template: "admin/admins/admin_error.js.erb" }
        end
      end
    end

    def edit
      @admin = Admin.find(params[:id])
    end

    def update
      @admin = Admin.find(params[:id])
      if @admin.update(admin_params)
        redirect_to admin_admin_path(@admin), notice: "Successful Updated"
      else
        render :edit
      end
    end

    def destroy
      @admin = Admin.destroy(params[:id])
      respond_to do |format|
        flash.now[:error] = "Delete"
        format.html { redirect_to admin_admins_url }
        format.js
      end
    end

    private
      def admin_only?
        unless  current_admin.admin?
          unless @user == current_admin
            redirect_to admin_dashboard_path, notice: "Access Denied"
          end
        end
      end

      def admin_params
        params.required(:admin).permit!
      end
  end
