class Admin::ArticlesController < ApplicationController
  before_action :authenticate_admin!

  def index
    @article = current_admin.articles.build
    @articles = Article.all
  end

  def show
    @article = Article.find_by_slug(params[:id])
  end

  def new
    
  end

  def create
    @article = current_admin.articles.build(article_params)
    if @article.save
      respond_to do |format|
        flash.now[:notice] = "Created Successful"
        format.html { redirect_to admin_articles_path }
        format.js
      end
    else
      respond_to do |format|
        flash.now[:alert] = "Please fill the field blank or article duplicated"
        format.html { redirect_to admin_articles_path}
        format.js
      end
    end
  end

  def edit
    @article = Article.find_by_slug(params[:id])
  end

  def update
    @article = Article.find_by_slug(params[:id])
    if @article.update(article_params)
      redirect_to admin_article_path(@article), notice: "Successful Updated"
    else
      render :edit
    end
  end

  def destroy
    @article = Article.destroy(params[:id])
    respond_to do |format|
      flash.now[:error] = "Deleted"
      format.html { redirect_to admin_articles_path }
      format.js
    end
  end

  private
    def admin_only
      unless current_admin.admin?
        unless @user == current_admin
          redirect_to admin_dashboard_path, notice: "Access denied"
        end
      end
    end

    def article_params
      params.required(:article).permit!
    end

end
