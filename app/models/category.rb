class Category < ApplicationRecord
  before_save :update_slug
  validates :category_name, uniqueness: true, presence: true, length: { maximum: 20 }

  belongs_to :admin
  has_many :articles
  validates :category_name, presence: true

  def update_slug
    self.slug = category_name.parameterize
  end

  def to_param
    slug
  end
end
