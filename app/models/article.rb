class Article < ApplicationRecord
    before_save :update_slug

    belongs_to :admin
    belongs_to :category

    def update_slug
        self.slug = title.parameterize
    end
    
    def to_param
        slug
    end
end
