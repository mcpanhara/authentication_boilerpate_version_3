Rails.application.routes.draw do

    root 'static_pages#home'
    resources :home
    devise_for :users, controllers: {
        sessions: 'users/sessions',
        registrations: "users/registrations"
      }


    # devise_for :admins, :controllers => { :registrations => :registrations }
    devise_for :admins, :skip => [:registrations]

    devise_scope :admin do
      get "/signin" => "devise/sessions#new", :as => "signin" # custom path to login/sign_in
    end

    devise_scope :user do
      get "/sign_in" => "devise/sessions#new", :as => "sign_in" # custom path to login/sign_in
      get "/sign_up" => "devise/registrations#new", :as => "sign_up" # custom path to login/sign_in
      get "users/verify/" => 'users/registrations#show_verify', as: 'verify'
      post "users/verify"
      post "users/resend"
    end

    as :admin do
      get 'admins/edit' => 'devise/registrations#edit', :as => 'edit_admin_registration'
      put 'admins' => 'devise/registrations#update', :as => 'admin_registration'
      patch 'admins' => 'devise/registrations#update', :as => 'update_admin_registration'
    end



    namespace :admin do
      root "dashboards#index"
      get 'dashboards' => "dashboards#index", :as => "dashboard"
      get 'invoices/:id' => 'orders#invoice', as: "invoice"


      resources :categories
      resources :articles
      resources :galleries
      resources :users
      resources :admins
    end

    get 'users' => 'users#index', :as => "users"
    post 'users' => "users#create"
    get 'signup' => "users#new", :as => "new_user"
    get "users/:id/edit" => "users#edit", :as => "edit_user"
    get "users/:id" => "users#show", :as => "user"
    patch "users/:id" => "users#update"
    put "users/:id" => "users#update"
    delete "users/:id" => "users#destroy"


end
